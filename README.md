# car2go

> Frontend technical challenge 

## Application Stack

The application is built on top of Vue's Webpack bolier plate template (http://vuejs-templates.github.io/webpack/)
In addition to that some supporting libraries were added.

1. axios - HTTP client
2. bulma - Css framework
3. vue-router - Router
4. vue2-leaflet - Maps
5. vuex - State managment


## Application Architecture
The application is sperated into different layers
- Api
    This is where all API Interactions reside including communication, mocking and any modification to the requests or responses    
- State
    This is where all the application data is stored and manipulated 
- Page Level Components [components/pages]
    These are top level component that forms the pages that the end custoemr sees
- Functional Components [components/*/car-finder]
    These are business function specifc components that performs the heavy lifiting such as communicating with the store, parse or modifiy data.
- Stupid Components [components/*/elements]
    These are user interface components that are context unaware and should be as flexiable and composable as possible.

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run all tests
npm test
```
