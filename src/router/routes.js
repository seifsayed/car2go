import HOME from "@/components/pages/home";

export default [
  {
    path: "/",
    name: "HOME",
    component: HOME
  }
];
