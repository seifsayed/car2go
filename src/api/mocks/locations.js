// Todo add mocks to their respective files and add proper configs


import axios from "../axios";
import MockAdapter from "axios-mock-adapter";


const mock = new MockAdapter(axios);
const canUseMocks = false;


if (canUseMocks) {
  // Mock any GET request to /users
  // arguments for reply are (status, data, headers)
  mock
    .onGet("/locations")
    .reply(200, [
      { id: 1, name: "Berlin" },
      { id: 2, name: "Hamburg" },
      { id: 3, name: "Luneburg" }
    ]);

  mock.onGet("/cars/1").reply(200, [
    {
      name: "BMW",
      seats: 10,
      location: {
        lat: 47.41322,
        lng: 1.219482
      }
    },
    {
      name: "MINI",
      seats: 2,
      location: {
        lat: 47.45322,
        lng: 1.219482
      }
    },
    {
      name: "TESLA",
      seats: 2,
      location: {
        lat: 47.57322,
        lng: 1.219482
      }
    }
  ]);

  mock.onGet("/cars/2").reply(200, [
    {
      name: "BMW",
      seats: 10,
      location: {
        lat: 40.41322,
        lng: 1.219482
      }
    },
    {
      name: "MINI",
      seats: 2,
      location: {
        lat: 40.45322,
        lng: 1.219482
      }
    },
    {
      name: "TESLA",
      seats: 2,
      location: {
        lat: 40.57322,
        lng: 1.219482
      }
    }
  ]);

  mock.onGet("/cars/3").reply(200, [
    {
      name: "BMW",
      seats: 10,
      location: {
        lat: 33.41322,
        lng: 1.219482
      }
    },
    {
      name: "MINI",
      seats: 2,
      location: {
        lat: 33.45322,
        lng: 1.219482
      }
    },
    {
      name: "TESLA",
      seats: 2,
      location: {
        lat: 33.57322,
        lng: 1.219482
      }
    }
  ]);
}
