import axios from "../axios";

export default {
  getCarsList(name) {
    return axios.get(`vehicles/${name}`);
  }
};
