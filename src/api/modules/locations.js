import axios from "../axios";

export default {
  getLocationsList() {
    return axios.get("locations");
  }
};
