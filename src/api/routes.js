const ROUTES = {
  CARS_LIST: "/cars/{id}",
  LOCATION_LIST: "/locations"
};

export default ROUTES;
