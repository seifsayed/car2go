import axios from "axios";

const instance = axios.create({
  baseURL: "https://car2go.now.sh/",
  timeout: 10000
});

export default instance;
