import carsApi from "../../api/modules/cars";

const Mutations = {
  UPDATE_LIST: "update_list",
  LOADING: "update_loading"
};

export const Actions = { FETCH_CARS_LIST: "fetch_cars_list" };

const cars = {
  namespaced: true,
  state: {
    isLoading: false,
    list: []
  },
  mutations: {
    [Mutations.UPDATE_LIST](state, update) {
      state.list = update;
    },
    [Mutations.LOADING](state, update) {
      state.isLoading = update;
    }
  },
  actions: {
    [Actions.FETCH_CARS_LIST]({ commit }, payload) {
      commit(Mutations.LOADING, true);
      carsApi
        .getCarsList(payload.name)
        .then((response) => {
          commit(Mutations.UPDATE_LIST, response.data);
          commit(Mutations.LOADING, false);
        })
        .catch(() => {
          commit(Mutations.LOADING, false);
        });
    }
  }
};

export default cars;
