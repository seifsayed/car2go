import locationsApi from "../../api/modules/locations";

const Mutations = {
  UPDATE_LIST: "update_list",
  LOADING: "update_loading",
  SELECT_LOCATION: "select_location"
};

export const Actions = {
  FETCH_LOCATIONS_LIST: "fetch_locations_list",
  SELECT_LOCATION: "select_locations"
};

const locations = {
  namespaced: true,
  state: {
    isLoading: false,
    list: [],
    selected: null
  },
  mutations: {
    [Mutations.UPDATE_LIST](state, update) {
      state.list = update;
    },
    [Mutations.LOADING](state, update) {
      state.isLoading = update;
    },
    [Mutations.SELECT_LOCATION](state, update) {
      state.selected = state.list.find(l => l.name === update);
    }
  },
  actions: {
    [Actions.FETCH_LOCATIONS_LIST]({ commit }) {
      commit(Mutations.LOADING, true);
      locationsApi
        .getLocationsList()
        .then((response) => {
          commit(Mutations.LOADING, false);
          commit(Mutations.UPDATE_LIST, response.data);
        })
        .catch(() => {
          commit(Mutations.LOADING, false);
        });
    },
    [Actions.SELECT_LOCATION]({ commit }, payload) {
      commit(Mutations.SELECT_LOCATION, payload);
    }
  }
};

export default locations;
